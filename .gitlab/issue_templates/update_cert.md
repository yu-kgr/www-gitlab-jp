### 有効期限

YYYY-MM-DD

### 更新手順

[how-to-update-certificate.md](doc/how-to-update-certificate.md) を参照。

### TODO

- [ ] ".well-known/acme-challenge" 以下のファイルを更新
- [ ] GitLabのSettingの証明書を更新
- [ ] ブラウザで証明書が更新されたことを確認
