---
layout: markdown_page
title: "Jenkins"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison
   - link to comparison page  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

### 概要
Jenkinsは世界中でもっとも人気のあるビルドの自動化、およびCI/CDツールの一つです。数百ものプラグインの機能を統合することで柔軟性を高め、あらゆるプロジェクトのビルド、デプロイ、自動化をサポートします。

### リソース
* Jenkins OSS ウェブサイト - [https://jenkins.io](https://jenkins.io)
* CloudBees Jenkins ウェブサイト - [https://www.cloudbees.com/products/cloudbees-core](https://www.cloudbees.com/products/cloudbees-core)

<!--
### Comments/Anecdotes
* From note on 2018-08-31 from CloudBees CTO and Jenkins creator Kohsuke Kawaguchi ([https://jenkins.io/blog/2018/08/31/shifting-gears/](https://jenkins.io/blog/2018/08/31/shifting-gears/)):
  - "Our Challenges. . . Service instability. . . Brittle configuration. . . Assembly required. . . Reduced development velocity. . ." (see above link for details on each)
  - "Path forward. . . Cloud Native Jenkins. . . continue the incremental evolution of Jenkins 2, but in an accelerated speed"
  - **Key takeaways**:
     - They plan on BREAKING backward compatibility in their upcoming releases
     - They plan on introducing a NEW flavor of Jenkins for Cloud native
     - If you’re a Jenkins user today, it’s going to be a rough ride ahead

* From Jenkins Evergreen project page (identified in Kohsuke letter above as key for changes that need to come) - [https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc)
   - "Pillars . . . Automatically Updated Distribution . . . Automatic Sane Defaults. . . Connected. . . Obvious Path to User Success"
   - "The "bucket of legos" approach is . . . not productive or useful for end-users [5] who are weighing their options between running Jenkins, or using a CI-as-a-Service offering such as Travis CI or Circle CI."
   - "existing processes around "Suggested Plugins", or any others for that matter, result in many "fiefdoms" of development rather than a shared understanding of problems and solutions which should be addressed to make new, and existing, users successful with Jenkins."
* From "Problem" definition page of Jenkins Evergreen project page ([https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc#problem](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc#problem)):
   > For novice-to-intermediate users, the time necessary to prepare a Jenkins environment "from scratch" into something productive for common CI/CD workloads, can span from hours to days, depending on their understanding of Jenkins and it’s related technologies. The preparation of the environment can also be very error prone and require significant on-going maintenance overhead in order to continue to stay up-to-date, secure, and productive.
   >
   > Additionally, many Jenkins users suffer from a paradox of choice [6] when it comes to deciding which plugins should be combined, in which ways, and how they should be configured, in order to construct a suitable CI/CD environment for their projects. While this is related to the problem which JEP-2 [7] attempted to address in the "Setup Wizard" introduced in Jenkins 2.0, Jenkins Evergreen aims to address the broader problem of providing users with a low-overhead, easily maintained, and solid distribution of common features (provided by a set of existing plugins) which will help the user focus on building, testing, and delivering their projects rather than maintaining Jenkins.

* Project analysis on Jenkins, pointed to by CloudBees documentation as proof for need of change - [https://ghc.haskell.org/trac/ghc/wiki/ContinuousIntegration#Jenkins](https://ghc.haskell.org/trac/ghc/wiki/ContinuousIntegration#Jenkins)
   > Pros
   >   - We can run build nodes on any architecture and OS we choose to set up.  
   >
   > Cons  
   >   - Security is low on PR builds unless we spend further effort to sandbox builds properly. Moreover, even with sandboxing, Jenkins security record is troublesome.
   >   - Jenkins is well known to be time consuming to set up.
   >   - Additional time spent setting up servers.
   >   - Additional time spent maintaining servers.
   >   - It is unclear how easy it is to make the set up reproducible.
   >   - The set up is not forkable (a forker would need to set up their own servers).
* GitLab blog on feedback about new Jenkins improvement efforts - [https://about.gitlab.com/2018/09/03/how-gitlab-ci-compares-with-the-three-variants-of-jenkins/](https://about.gitlab.com/2018/09/03/how-gitlab-ci-compares-with-the-three-variants-of-jenkins/)

### Resources
* [Jenkins Website](https://jenkins.io/) - Open Source project website
* [CloudBees Website](https://www.cloudbees.com/) - CloudBees a distribution of Jenkins with support and enterprise capabilities

### Pricing
* Jenkins OSS
   - No cost (and Open Source)
   - But Total Cost of Ownership is not zero, given maintenance requirements
* CloudBees Jenkins
 (vague) - https://www.cloudbees.com/products/pricing
   - CloudBees Core - Jenkins distribution with upgrade assistance on monthly incremental upgrades, cloud native architecture, centralized management, 24/7 support and training, enterprise-grade security and multi-tenancy, and plugin compatibility testing
      - starting at $20k/year for 10 users, with tiered pricing for lower per-user cost for larger organizations
-->
