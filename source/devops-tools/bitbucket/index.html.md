---
layout: markdown_page
title: "Atlassian BitBucket"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison
   - link to comparison page  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary
Atlassian Bitbucket gives teams Git code management, but also one place to plan projects, collaborate on code, test and deploy. It is marketed in the SaaS form (Bitbucket Cloud) and in a self-managed version (Bitbucket Server), however they are not the same product. Bitbucket Server is simply a [re-branding of Stash](https://www.atlassian.com/blog/archives/atlassian-stash-enterprise-git-repository-management). The two products are completely different code bases, written in two different languages ([Cloud in Python, Server in Java](https://en.wikipedia.org/wiki/Bitbucket)) and do not maintain feature parity. 

Bitbucket supports  Mercurial or Git, but not SVN. GitLab does not support Mercurial or SVN. 

GitLab is a single application for the complete DevOps lifecycle with built-in project management, source code managemetn, CI/CD, monitoring and more. Bitbucket only does source code managment. You would need to use Atlassian Jira to get project managment, and Bamboo for CI/CD and Atlassian does not provide a monitoring solution. Addditionally, GitLab Ultimate comes with robust built-in security capabilities such as SAST, DAST, Container Scanning, Dependency Scanning, and more. Bitbucket does not support these capabilities, and Atlassian does not have a product for them. 

GitLab also offers a "prem" self-managed and "cloud" SaaS solution. GitLab runs the same exact code on it's SaaS platform that it offers to it's customers. This means customers can migrated from self-hosted to SaaS and back relatively easily and each solution maintains feature parity.

### Comments/Anecdotes
* GitLab  employee observation from a recent [HackerNews article](https://news.ycombinator.com/item?id=17908553)
   > Atlassian put the B-team on BitBucket Server and it isn't getting many updates because it is written in a different programming language. This stresses the value of having codebase parity accross products.
* Disparity between BitBucket Server and BitBucket Cloud for a top customer requested feature took Atlassian over 2 years to acknowledge. Customers found out the feature existed in Server but not Cloud once moving tp Cloud. Still not resolved. - [https://bitbucket.org/site/master/issues/12833/branching-models-for-bb-cloud#comment-45982415](https://bitbucket.org/site/master/issues/12833/branching-models-for-bb-cloud#comment-45982415)

### Resources
* [Atlassian BitBucket Website](https://bitbucket.org/product)

### Pricing
- [Bitbucket.org](https://bitbucket.org/product/pricing)
  * Offers free private repos while commercial plans charge per user
  * Free tier - $0 - Unlimited private repos, Jira Software integration, Projects Pipelines (50 build mins/month), 1GB/month limit on file storage
  * Standard tier - $2/user/month (min $10/month) - Same as Free + 500 build mins/month + 5GB file storage/month
  * Premium tier - $5/usr/month (min $25/month) - Standard + some advanced features + 1000 build mins/month + 10GB file storage/month

- [BitBucket Server Enterprise](https://bitbucket.org/product/enterprise)
  * Server - starting $2k perpetual (25 users, ppu drops roughly every 2x previous tier), includes year maintenance, single server, unlimited priv+pub repos
  * Data Center - $1800/yr (25 users , ppu drops roughly every 2x previous tier) includes annual maintenance, Server + HA, DR, mirroring, SAML 2.0
  * Must buy Data Center if over 2k users.

### Comparison
- [Compare BitBucket Data Center features to GitLab](https://about.gitlab.com/comparison/bitbucket-data-center-vs-gitlab.html)
- [Compare BitBucket.org features to GitLab](https://about.gitlab.com/comparison/bitbucket-org-vs-gitlab.html)