---
release_number: "11.8" # version number - required
title: "GitLab 11.8がリリース、SASTのJavaScript対応、Pagesのサブグループ対応、エラートラッキング" # short title - required
author: Hiromi Nozawa # author name and surname - required
author_gitlab: hir0mi # author's gitlab.com username - required
image_title: '/images/11_8/11_8-cover-image.jpg' # cover image - required
description: "GitLab 11.8がリリースされました。SASTのJavaScript対応、Pagesのサブグループ対応、エラートラッキングなど様々な点が改善されています。" # short description - required
twitter_image: '/images/tweets/gitlab-11-8-released.png' # social sharing image - not required but recommended
categories: releases # required
layout: release # required
featured: yes
header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
original_url: https://about.gitlab.com/2019/02/22/gitlab-11-8-released/
---

## SAST での JavaScript カバレッジ
{: .intro-header}

GitLab の静的アプリケーションセキュリティテスト (SAST) はソースコードをスキャンし、パイプラインの早い段階で潜在的なセキュリティの脆弱性を検出するのに役立ちます。11.8 では [JavaScript に対する SAST サポート](./#sast-support-for-javascript) を追加しました。これは既存の node.js サポートの上に構築されています。これにより、静的スクリプトや HTML などの任意の JavaScript ファイルをスキャンできるようになります。DevSecOps における最も重要なプラクティスは、コミットごとにコードの変更をスキャンすることです。この変更によって、最も人気のある Web 言語の 1 つをカバーし JavaScript のリスクをできるだけ早く発見できるようにします。

## GitLab Pages のサブグループとテンプレート
{:.intro-header}

今回のリリースでは GitLab Pages は 2 つの重要な改善点を含めて大幅に改善されました。まず、[GitLab Pages によるサブグループ内のプロジェクトのサポート](./#pages-support-for-subgroups) を導入し、これらのプロジェクトでコンテンツを簡単に Web に公開できるようにしました。GitLab 11.8 ではまた最も人気のある [Pages 用テンプレート](./#create-pages-sites-in-one-click-using-bundled-templates) をバンドルしているので、ユーザはワンクリックで始めることができます。


## Sentry によるエラートラッキング
{: .intro-header}

アプリケーションエラーはアプリケーションの状態についての重要な情報を提供し、ユーザからの報告を待たずに問題を検知するのに役立ちます。GitLab 11.8 では、プロジェクト内で直接 [最新のエラーを表示](./#error-tracking-with-sentry) できるようになりました。これにより、エラーを簡単に見つけて対処することができます。
 
## その他の多くの素晴らしい機能！
{:.intro-header}

今回のリリースには非常に多くの素晴らしい機能があるので、もう少しピックアップしたいと思います:

* [**マージリクエストの承認ルール**](./#merge-request-approval-rules): 特定のユーザ、グループ、またはロールにかかわらず、誰が変更を承認する必要があるかについてのルールを簡単に定義できます。 GitLab.com ではまもなく利用可能になり、GitLab インスタンスの管理者は自分自身で有効にできるようになります。

* [**環境ごとのフィーチャーフラグ**](./#feature-flags-for-environments): 以前は全ての環境共通でフィーチャーフラグがオンまたはオフになっていましたが、これは望ましくありません！環境ごとにフィーチャーフラグを選択的に有効にできるようになりました。GitLab.com では今日から使えるようになっていて、GitLab インスタンスの管理者は自分自身で有効にできます。

* [**改善された Squash コミットメッセージ**](#improved-squash-commit-messages): 素晴らしいコミットメッセージを作り上げることを楽しんでいる人にとって、物事をきちんと保つためにそれらが Squash されたコミットで失われるのを見るのは悲しいことです。11.8では Squash されたコミットは自動的に最初の複数行コミットメッセージを利用するようになりました。また、さらにそれらのコミットメッセージを改善するために上書きすることもできます。

