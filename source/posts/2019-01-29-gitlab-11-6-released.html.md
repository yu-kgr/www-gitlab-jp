---
release_number: "11.6"
title: "GitLab 11.6がリリース、サーバレス、およびグループレベルクラスタ"
author: Hiroyuki Sato
author_gitlab: hiroponz
author_twitter: hiroponz79
image_title: '/images/11_6/11_6-cover-image.jpg'
description: "GitLab 11.6がリリースされました。サーバレス機能やグループレベルのKubernetesクラスタ連携により、クラウドネイティブな開発を容易にします。"
twitter_image: '/images/tweets/gitlab-11-6-released.png'
categories: releases
layout: release
featured: yes
original_url: https://about.gitlab.com/2018/12/22/gitlab-11-6-released/
---

## GitLabでサーバレスワークロードをあらゆるクラウドにデププロイ
{: .intro-header}


サーバレスコンピューティングにより、コードの実行に必要なクラウドのリソースを動的に割り当てることができ、リソースの割り当てと配置を最適化できます。
サーバレスコンピューティングによって、開発者はコードが実行されるインフラを意識せずに、課題を解決するためのコードを書くことに集中できます。

[GitLab Serverless](https://about.gitlab.com/product/serverless/){:target="_blank"}
は、KubernetesをベースとするプラットフォームのKnativeを使用して、ビルドやデプロイといったサーバレスワークロードを管理します。
この機能は、開発者がサーバレスワークロードを簡単に管理できるようにするための、扱いやすいインターフェースを提供します。
ビジネス的には、マルチクラウドで利用できるサーバレスコンピューティングによって、特定のクラウドプロバイダへのベンダーロックインを防ぐことができます。

## GitLabはクラウドネイティブアプリケーションの開発を容易にします
{:.intro-header-h3}

GitLabの組み込みの
[コンテナレジストリ](https://docs.gitlab.com/ee/user/project/container_registry.html){:target="_blank"}
と
[Kubernetes連携](https://docs.gitlab.com/ee/user/project/integrations/kubernetes.html){:target="_blank"}
により、コンテナを利用したクラウドネイティブな開発を容易に開始できます。
11.6では、グループとサブグループ内のすべてのプロジェクトで利用できる
[グループKubernetesクラスタ](#kubernetes-clusters-for-groups-beta)
を作成できるようになりました。
この機能により、クラウドネイティブな開発に必要な環境構築作業を省力化でき、開発者はアプリケーションの開発に集中することができます。

## その他にもたくさんの機能が追加されました
{:.intro-header-h3}

[変更の提案](#suggested-changes)、
[Web IDE用のWeb Terminal](#web-terminal-for-web-ide-beta)、
[グループセキュリティダッシュボードの脆弱性チャート](#vulnerability-chart-for-group-security-dashboards)
などの便利な機能が追加されました。
マージリクエストの差分に対して、コメントで変更の提案ができます。変更の提案は1クリックで受け入れができ、チームでの開発を効率化できます。
Web IDEにターミナル機能が搭載されました。これにより、Web IDEでサーバサイドでのコード評価が可能になりました。
先月リリースされた
[グループセキュリティダッシュボード](https://docs.gitlab.com/ee/user/group/security_dashboard/){:target="_blank"}
には、脆弱性チャートが追加されました。これにより、脆弱性数の日々の変化を可視化できます。
