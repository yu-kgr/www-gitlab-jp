---
release_number: "11.4"
title: "GitLab 11.4がリリース、レビュー機能やフィーチャーフラグ"
author: Hiroyuki Sato
author_gitlab: hiroponz
author_twitter: hiroponz79
image_title: '/images/11_4/11_4-cover-image.jpg'
description: "GitLab 11.4がリリースされました。マージリクエストでのコードレビューの改善やフィーチャーフラグの導入など様々な点が改善されています。"
twitter_image: '/images/tweets/gitlab-11-4-released.png'
categories: releases
layout: release
header_layout_dark: true
original_url: 'https://about.gitlab.com/2018/10/22/gitlab-11-4-released/'
featured: yes
---

2018年10月22日にリリースされたGitLab 11.4では、[マージリクエストのレビュー機能](#merge-request-reviews)が強化されたことに加えて、
[変更されたファイルをツリー形式で表示](#file-tree-for-browsing-merge-request-diff)するように改善されたことによって、コードレビューを効率的に実施できるようになりました。
[フィーチャーフラグ機能(アルファ版)](#create-and-toggle-feature-flags-for-your-applications-alpha)が導入されました。
[PostgreSQLのマイグレーション](#support-postgresql-db-migration-and-initialization-for-auto-devops)や[定期的なインクリメンタルロールアウト](#add-timed-incremental-rollouts-to-auto-devops)によって、
Auto DevOpsやCIがより強力になりました。
さらに、[Gitプロトコル v2](#git-protocol-v2)をサポートしたことで、Gitの動作が高速化されました。

## コードレビュー
{: .intro-header}

[マージリクエストのレビュー機能](#merge-request-reviews)の強化によって、マージリクエストでのコメントのノイズを低減することができます。
レビュワーは複数のコメントを一つにまとめて投稿することができるようになりました。
これにより、複数に分かれて届いた通知をまとめることができるので、プロジェクトの参加者は、より効率的に変更を追跡できます。

適切な担当者がコードの変更をレビューし承認することにより、コードの品質を高めることができます。
11.3のリリースで導入された`CODEOWNERS`ファイルの情報に基づいて、[マージリクエストをレビューし承認すべき担当者が提案](#suggest-code-owners-as-merge-request-approvers)されるようになりました。
これによって、変更のレビューと承認を素早く効率的に実施できます。

さらに、[修正されたファイルをツリー形式](#file-tree-for-browsing-merge-request-diff)で表示するように改善され、
より簡単に素早く修正されたファイルを見つけて移動できるようになりました。

## フィーチャーフラグ
{: .intro-header}

機能の切替を簡単にできる、[フィーチャーフラグ機能のアルファ版](#file-tree-for-browsing-merge-request-diff)がリリースされました。
これにより、新機能をリリースする際のリスクを低減することができます。

## Auto DevOpsとCI/CD
{: .intro-header}

[`gitlab-ci.yml`ファイルをインクルードして利用](#move-ability-to-use-includes-in-codegitlab-ciymlcode-from-starter-to-core)できるプランをStarterからCoreへ変更しました。
これにより、すべてのチームがCI/CDパイプラインをより効率的に管理できるようになります。

## その他の改善
{: .intro-header}

[プロフィールページのレイアウトの変更](#new-user-profile-page-overview)、[プロフィールステータスのアクセスを改善](#set-and-show-your-status-message-within-the-user-menu)、
[`@mentions`のハイライト](#highlight-codementionscode-for-yourself-distinctly)、[新しいクイックアクション](#lock-discussion-quick-action)、[エピックのクローズ](#close-epics)など多くの改善が含まれています。

GitLab 11.4のすべての機能の詳細をご覧ください。
