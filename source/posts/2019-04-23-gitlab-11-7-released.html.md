---
release_number: "11.7"
title: "GitLab 11.7がリリース、リリース機能、 子エピックの他層化、 NPMレジストリ"
author: Yuko Takano
author_gitlab: takano_cl
image_title: '/images/11_7/11_7-cover-image.jpg'
description: "GitLab 11.7がリリースされました。リリース機能、子エピックの他層化、NPMレジストリが実装！"
twitter_image: '/images/tweets/gitlab-11-7-released.png'
categories: releases
layout: release
featured: yes
header_layout_dark: true
original_url: https://about.gitlab.com/2019/01/22/gitlab-11-7-released/
---

## リリース管理がより簡単に
{: .intro-header}

GitLab 11.7では、GitLab Coreで[リリース機能](#publish-releases-for-your-projects)が利用できるようになります。
ユーザは、ソースコードのみならず、全ての成果物を含めたリリース時のスナップショット を保持することができるようになりました。
これにより、ソースコードやビルドの成果物、他のメタデータやコードのリリースバージョンに関連する成果物などを手動で収集管理する必要がなくなります。
加えて、このリリース機能は将来的により幅広く、パワフルなリリースオーケストレーションになるべく、準備されます。

## ポートフォリオ管理がより複雑なWBSをサポート
{: .intro-header}

[子エピックの多層化](#multi-level-child-epics) はUltimateで利用できる、GitLabポートフォリオ管理の最新機能です。
子エピックでは複数階層のWBSを利用できるようになり、より複雑な案件や作業計画の一助となるでしょう。
課題とエピックの両方を含むエピックを作ることができるようになり、この構造によって、計画と実装にあたることができる課題をダイレクトに結びつけることができます。

## NPMレジストリを使用してJavaScriptでの開発を効率化
{: .intro-header}

GitLab 11.7のPremiumでは、NPMレジストリをGitLab内部に持てるようになりました。それにより、NPMパッケージをプロジェクト横断的に共有し、またNPMパッケージをバージョン管理することが可能になります。
パッケージ名を伝えるだけで、あとはNPMとGitLabにおまかせできます。すべてが1つのインターフェースで完結します！

## その他の機能
{: .intro-header}

月次リリース記事のトップで「どの機能を取り上げるか」を選ぶのは毎回とても難しい作業になっています。以下は、その他の素晴らしい機能のご紹介です。

* [**パッチファイルで脆弱性を修復**](#remediate-vulnerability-with-patch-file):ご存知の通り、GitLabのセキュリティ機能では、脆弱性を検知することができます。GitLab 11.7では、さらに、脆弱性を修復する機能が盛り込まれました。そして、Node.jsプロジェクトには、Yarnでの解決方法の提案も行われます。これはGitLabが提供する初めての修復タイプ機能であり、これからますます発展が見込まれます！

* [**APIによるKubernatesの構築**](#api-support-for-kubernetes-integration): もしあなたがたくさんのKubernatesクラスターを構築する業務にあたっているとしたら、GitLabのKubernetes APIを利用して、あなたの手作業を減らし、日々の仕事をすいぶん楽にできるでしょう！

* [**プロジェクト横断でのパイプラインの表示**](#cross-project-pipeline-browsing): プロジェクトをまたいでパイプラインを確認できるようになりました。この機能を使えば、どんな情報でもすぐに得られるでしょう！

GitLab 11.7の全機能についてみていきましょう！
