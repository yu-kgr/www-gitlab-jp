require 'extensions/breadcrumbs'

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  # blog.custom_collections = {
  #   categories: {
  #     link: '/blog/categories/{categories}/index.html',
  #     template: '/category.html'
  #   }
  # }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Proxy comparison PDF pages
# Used for old pages, can delete once we 301 redirect old content: https://gitlab.com/gitlab-com/www-gitlab-com/issues/2543
data.features.comparisons.each do |key, comparison|
  file_name = key.dup.tr('_', '-')
  proxy "/comparison/pdfs/#{file_name}.html", '/comparison/pdfs/template.html', locals: {
    comparison_block: comparison,
    key_one: comparison.product_one.to_sym,
    key_two: comparison.product_two.to_sym
  }, ignore: true
end

# Proxy Comparison html and PDF pages
data.features.competitors.each_key do |competitor|
  next if competitor[0..6] == 'gitlab_'
  file_name = "#{competitor}-vs-gitlab".tr('_', '-')
  proxy "/comparison/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: competitor,
    key_two: 'gitlab_ultimate'
  }
  proxy "/comparison/pdfs/#{file_name}.html", '/comparison/pdfs/template.html', locals: {
    key_one: competitor,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  set :build_dir, 'public'
  set :base_url, '/www-gitlab-jp' # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :relative_assets # Use relative URLs
  activate :minify_css
  activate :minify_javascript
  activate :minify_html
end

configure :development do
  activate :livereload
end

ignore '/templates/*'
ignore '/direction/template.html'
ignore '/direction/product-vision/template.html'
ignore '/includes/*'
ignore '/releases/template.html'
ignore '/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
